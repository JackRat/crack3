#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be
int compareCount = 0;

// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char *plainpass;
    char *hashpass;
};



// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.
//Load Dictionary First, Then Sort It, Then Bsearch, Then It All Runs.
struct entry *read_dictionary(char *filename, int *size)
{
    struct entry *pass = malloc(100 * sizeof(struct entry));
    FILE *in = fopen(filename, "r");
    // TODO: Check to make sure file was opened
    if(!in)
    {
        perror("Can't open dictionary");
	    exit(1);
    }

    char word[50];
    int capacity = 100;
    int count = 0;
    while(fgets(word, 100, in) != NULL)
    {

        char *nl = strchr(word, '\n');
        if(nl) *nl = '\0';

	    printf("count = %d, word = %s\n", count, word);

        char *str = malloc(strlen(word)+1);
        strcpy(str, word);

        pass[count].plainpass = str;

        char *hash = md5(word, strlen(word));

        pass[count].hashpass = hash;

        if(count == capacity)
        {
            capacity += 100;
            pass = realloc(pass, (capacity * sizeof(struct entry)));
        }
        count++;
    }
    size = (int *)sizeof(pass); 
    /*This neds to tell the main function how big the password file is like if it is 1000 from rockyou1000 or 2000 from rockyou2000 etc
    I donn't know ^ if that is correct yet */
    fclose(in);
    return pass;
}



int comparefnc(const void * a, const void * b)
{
    compareCount++;
    char *aa = (char *)a;
    struct entry *bb = (struct entry *)b;
    return strcmp(aa, bb[compareCount].hashpass);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the dictionary file into an array of entry structures FIRST IS (FILENAME, POINTER TO SIZE) for read_dictionary(NULL, NULL)
    int size;
    struct entry *dict = read_dictionary(argv[2], &size);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.

    qsort(dict, size, sizeof(struct entry), comparefnc);
    //I THINK THIS 100   ^ is going to be size, the variable defined near the top of the function.


    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".

    //struct entry *found = bsearch("f806fc5a2a0d5ba2471600758452799c", dict, 0, 0, NULL);

    // TODO
    // Open the hash file for reading.
    FILE *in = fopen(argv[1], "r");
    // // TODO
    // // For each hash, search for it in the dictionary using
    // // binary search.
    // // If you find it, get the corresponding plaintext dictionary word.
    // // Print out both the hash and word.
    // // Need only one loop. (Yay!)
    char word[101];
    int count = 0;
    while(fgets(word, 101, in) != NULL)
    {
        char *nl = strchr(word, '\n');
        if(nl) *nl = '\0';
        // printf("%s", word);
        struct entry *found = bsearch(&word, dict, size, sizeof(struct entry), comparefnc);
        //I THINK THIS 100 is going to be size, the variable defined near the top of the function.
        if(found) printf("Found: %c, %s\n", *found->hashpass, dict[count].plainpass);
        else printf("Not found ");
        count++;
    }
    printf("\n");
    //     //GETTING CLOSER, NEED TO GET THE BSEARCH TO WORK.
    
}
